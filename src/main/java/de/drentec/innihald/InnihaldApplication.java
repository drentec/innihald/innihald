package de.drentec.innihald;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InnihaldApplication {

    public static void main(String[] args) {
        SpringApplication.run(InnihaldApplication.class, args);
    }

}
